package crystal.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CurrentGameTest {

	CurrentGame game;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		game = new CurrentGame();
	}

	@After
	public void tearDown() throws Exception {
		game = null;
	}
	
	@Test
	public void gameIsLostWithTooFewPoints() {
		game.setCurrentPoints(100);
		assertFalse(game.wins());
	}
	
	@Test
	public void gameIsWonWithEnoughPoints() {
		game.setCurrentPoints(10000);
		assertTrue(game.wins());
	}

}
