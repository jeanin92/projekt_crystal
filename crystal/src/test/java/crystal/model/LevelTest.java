package crystal.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class LevelTest {
	Level tester;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLastLevelReached() {
		tester = Level.LEVEL5;
		assertTrue(tester.isLastLevel());
	}
	
	@Test
	public void testLastLevelNotReached() {
		tester = Level.LEVEL4;
		assertFalse(tester.isLastLevel());
	}

}
