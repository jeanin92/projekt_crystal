package crystal.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class BlockTest {
	private Block tester;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		tester = new Block(Colour.RED);
	}

	@After
	public void tearDown() throws Exception {
		tester = null;
	}

	@Test
	public void testNeighboursNotNull() {
		tester.setUpNeighbour(new Block(Colour.RED));
		tester.setRightNeighbour(new Block(Colour.YELLOW));
		tester.setDownNeighbour(new Block(Colour.GREEN));
		tester.setLeftNeighbour(new Block(Colour.BLUE));
		assertNotNull(tester.getUpNeighbour());
		assertNotNull(tester.getRightNeighbour());
		assertNotNull(tester.getDownNeighbour());
		assertNotNull(tester.getLeftNeighbour());
	}
	
	@Test
	public void testIsClickableOneSimilarNeighbour() {
		tester.setUpNeighbour(new Block(Colour.RED));
		tester.setRightNeighbour(new Block(Colour.YELLOW));
		tester.setDownNeighbour(new Block(Colour.GREEN));
		tester.setLeftNeighbour(new Block(Colour.BLUE));
		assertTrue(tester.isClickable());
	}
	
	@Test
	public void testIsClickableNoSimilarNeighbour() {
		tester.setUpNeighbour(new Block(Colour.ORANGE));
		tester.setRightNeighbour(new Block(Colour.YELLOW));
		tester.setDownNeighbour(new Block(Colour.GREEN));
		tester.setLeftNeighbour(new Block(Colour.BLUE));
		assertFalse(tester.isClickable());
	}
	
	@Test
	public void testIsClickableNoSimilarNeighbourWithNull() {
		tester.setRightNeighbour(new Block(Colour.YELLOW));
		tester.setDownNeighbour(new Block(Colour.GREEN));
		tester.setLeftNeighbour(new Block(Colour.BLUE));
		assertNull(tester.getUpNeighbour());
		assertFalse(tester.isClickable());
	}
	
	@Test
	public void testIsClickableSimilarNeighbourWithNull() {
		tester.setRightNeighbour(new Block(Colour.RED));
		tester.setDownNeighbour(new Block(Colour.GREEN));
		tester.setLeftNeighbour(new Block(Colour.BLUE));
		assertTrue(tester.isClickable());
	}

}
