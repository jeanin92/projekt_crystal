package crystal.controller;

import static org.junit.Assert.*;

import java.sql.Date;
import java.util.List;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import crystal.controller.DBOperator;
import crystal.entities.Game;
import crystal.model.CurrentGame;
import crystal.model.Level;

public class DBOperatorTest {
	static DBOperator tester;

	@Before
	public void setUp() throws Exception {
		tester = new DBOperator("hibernateTest.cfg.xml");
	}

	@After
	public void tearDown() throws Exception {
		Session session = tester.getSession();
		session.beginTransaction();
		session.createQuery("DELETE FROM Game").executeUpdate();
		session.getTransaction().commit();
		tester = null;
	}
	
	@Test
	public void testInit(){
		assertNotNull("Factory must not be null", tester.getFactory());
		assertNotNull("Session must not be null", tester.getSession());
	}

	@Test
	public void testGetRanking() {
		insertDummies();
		
		List<Game> ranking = tester.getRanking();		
		assertNotNull("Ranking must not be null", ranking);	
		assertEquals("Tabelle muss 3 Eintr��ge enthalten", 3, ranking.size());
		assertEquals("Dummy 3 muss an erster stelle stehen", "dummy3", ranking.get(0).getUser());
		assertEquals("Erster Eintrag muss auf Rang 1 sein", 1, ranking.get(0).getRank());
		assertEquals("Zweiter eintrag muss auf Rang 2 sein", 2, ranking.get(1).getRank());
		assertEquals("Dummy 1 und 2 m��ssen den selben Rang haben", ranking.get(2).getRank(), ranking.get(1).getRank());
	}
	
	@Test
	public void testSaveOneGame(){
		CurrentGame currentGame = new CurrentGame();
		currentGame.setCurrentLevel(Level.LEVEL2);
		currentGame.setTotalPoints(4200);
		tester.getGame().setUser("test");
		assertEquals("ranking"+GameController.FORCEPAGEREDIRECT, tester.saveGame(currentGame));
		Session session = tester.getSession();
		session.beginTransaction();
		List<Game> result = session.createQuery("from Game where user='test' and points=4200 and level=2").list();
		session.getTransaction().commit();
		assertNotSame(0, result.size());		
	}
	
	private void insertDummies(){
		Session session = tester.getSession();
		session.beginTransaction();
		session.save(new Game("dummy1", 100, 1, new Date(2014, 01, 01)));
		session.save(new Game("dummy2", 100, 1, new Date(2014, 01, 01)));
		session.save(new Game("dummy3", 150, 1, new Date(2014, 01, 01)));	
		session.getTransaction().commit();
	}

}
