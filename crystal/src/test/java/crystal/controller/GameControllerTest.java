package crystal.controller;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import crystal.model.Board;
import crystal.model.CurrentGame;
import crystal.model.Level;

public class GameControllerTest {
	GameController tester;

	@Before
	public void setUp() throws Exception {
		tester = new GameController();
		tester.init();
	}

	@After
	public void tearDown() throws Exception {
		tester = null;
	}

	@Test
	public void testStartNewLevel() {
		Board initialBoard = tester.getGame().getBoard();
		tester.startNewLevel();
		
		assertEquals(Level.LEVEL2, tester.getGame().getCurrentLevel());
		assertEquals(0, tester.getGame().getCurrentPoints());
		assertEquals(Board.BOARDLENGTH, tester.getGame().getCurrentNumberOfBlocks());
		assertNotSame(initialBoard, tester.getGame().getBoard());
	}
	
	@Test
	public void testStartNewGame(){
		CurrentGame firstGame = tester.getGame();
		assertEquals("game?faces-redirect=true", tester.goToGamePage());
		assertNotSame(firstGame, tester.getGame());
	}
	
	@Test
	public void testCancelGameLevel1(){
		assertEquals(Level.LEVEL1, tester.getGame().getCurrentLevel());
		assertEquals("noFinishedLevel?faces-redirect=true", tester.cancelGame());
	}
	
	@Test
	public void testCancelGameLevel3(){
		tester.getGame().setCurrentLevel(Level.LEVEL3);		
		assertEquals("finalResult?faces-redirect=true", tester.cancelGame());
		assertEquals(Level.LEVEL2, tester.getGame().getCurrentLevel());
	}
	
	@Test
	public void testGoToFinalResultForLastLevel(){
		tester.getGame().setCurrentLevel(Level.LEVEL5);
		assertEquals("finalResult?faces-redirect=true", tester.goToResult());
	}
	
	@Test
	public void testGoToFinalResultIfYouLose(){
		tester.getGame().setCurrentLevel(Level.LEVEL3);
		tester.getGame().setCurrentPoints(10);
		assertEquals("finalResult?faces-redirect=true", tester.goToResult());
	}
	
	@Test
	public void testGoToLevelResultIfYouWin(){
		tester.getGame().setCurrentLevel(Level.LEVEL2);
		tester.getGame().setCurrentPoints(10000);
		assertEquals("levelResult?faces-redirect=true", tester.goToResult());
	}
	
	@Test
	public void testGetPreviousLevelName(){
		assertEquals("Level 0", tester.returnPreviousLevelName());
	}
	
	@Test
	public void testGetNextLevelName(){
		assertEquals("Level 2", tester.returnNextLevelName());
	}

}
