package crystal.services;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import crystal.model.Board;
import crystal.model.CurrentGame;
import crystal.model.Level;

public class GameStatusUpdaterTest {

	private GameStatusUpdater updater;
	private CurrentGame currentGame;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		currentGame = new CurrentGame();
		updater = new GameStatusUpdater(currentGame);
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void PointsUpdatedCorrectly() {
		currentGame.setCurrentPoints(123);
		updater.updatePoints(20);
		assertEquals(226, currentGame.getCurrentPoints());
	}
	
	@Test
	public void blockCountUpdatedCorrectly() {
		currentGame.setCurrentNumberOfBlocks(176);
		updater.updateBlockCount(23);
		assertEquals(153, currentGame.getCurrentNumberOfBlocks());
	}
	
	@Test
	public void finishLevelIsCorrectForWinningWithBonus() {
		currentGame.setTotalPoints(2010);
		currentGame.setCurrentPoints(1500);
		currentGame.setCurrentLevel(Level.LEVEL1);
		updater.finishLevel();
		assertTrue(currentGame.isFinished());
		assertEquals(3710, currentGame.getTotalPoints());
	}
	
	@Test
	public void finishLevelIsCorrectForWinningWithoutBonus() {
		currentGame.setTotalPoints(2010);
		currentGame.setCurrentPoints(1500);
		currentGame.setCurrentLevel(Level.LEVEL1);
		currentGame.setCurrentNumberOfBlocks(10);
		updater.finishLevel();
		assertTrue(currentGame.isFinished());
		assertEquals(3510, currentGame.getTotalPoints());
	}
	
	@Test
	public void finishLevelIsCorrectForLosing() {
		currentGame.setTotalPoints(2010);
		currentGame.setCurrentPoints(400);
		currentGame.setCurrentLevel(Level.LEVEL1);
		updater.finishLevel();
		assertTrue(currentGame.isFinished());
		assertEquals(2010, currentGame.getTotalPoints());
	}
	
	@Test
	public void statusResettedCorrectlyBeforeNewLevel() {
		currentGame.setCurrentNumberOfBlocks(34);
		currentGame.setCurrentPoints(98);
		updater.setStatusOfLevel();
		assertEquals(Board.BOARDLENGTH, currentGame.getCurrentNumberOfBlocks());
		assertEquals(0, currentGame.getCurrentPoints());
	}
	
	@Test
	public void levelIncrementedCorrectly() {
		currentGame.setCurrentLevel(Level.LEVEL2);
		updater.incrementLevel();
		assertEquals(Level.LEVEL3, currentGame.getCurrentLevel());
	}
	
	@Test
	public void lastLevelNotIncremented() {
		currentGame.setCurrentLevel(Level.LEVEL5);
		updater.incrementLevel();
		assertEquals(Level.LEVEL5, currentGame.getCurrentLevel());
	}

}
