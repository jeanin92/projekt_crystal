package crystal.services;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import crystal.model.Block;
import crystal.model.Colour;

public class BlockRemoverTest {
	BlockRemover tester;
	Block clickedBlock;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		tester = new BlockRemover();
		clickedBlock = new Block(Colour.RED);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRemoveNeighboursNoMatching() {
		clickedBlock.setUpNeighbour(new Block(Colour.GREEN));
		clickedBlock.setRightNeighbour(new Block(Colour.YELLOW));
		clickedBlock.setDownNeighbour(new Block(Colour.BLUE));
		Block leftNeighbour = new Block(Colour.RED);
		leftNeighbour.setVisible(false);
		clickedBlock.setLeftNeighbour(leftNeighbour);
		assertEquals(1, tester.removeSameColouredNeighboursOf(clickedBlock));
	}
	
	@Test
	public void testRemoveNeighboursTwoMatchings() {
		clickedBlock.setUpNeighbour(new Block(Colour.RED));
		clickedBlock.setRightNeighbour(new Block(Colour.YELLOW));
		clickedBlock.setDownNeighbour(new Block(Colour.BLUE));
		clickedBlock.setLeftNeighbour(new Block(Colour.RED));
		assertEquals(3, tester.removeSameColouredNeighboursOf(clickedBlock));
	}
	
	@Test
	public void testRemoveNeighboursThreeMatchingsSecondLayer() {
		clickedBlock.setUpNeighbour(new Block(Colour.RED));
		clickedBlock.setRightNeighbour(new Block(Colour.YELLOW));
		clickedBlock.setDownNeighbour(new Block(Colour.BLUE));
		Block leftNeighbour = new Block(Colour.RED);
		leftNeighbour.setDownNeighbour(new Block(Colour.RED));
		clickedBlock.setLeftNeighbour(leftNeighbour);
		assertEquals(4, tester.removeSameColouredNeighboursOf(clickedBlock));
	}

}
