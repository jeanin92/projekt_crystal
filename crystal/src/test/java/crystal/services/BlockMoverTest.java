package crystal.services;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import crystal.model.Block;
import crystal.model.Board;
import crystal.model.Colour;

public class BlockMoverTest {

	private BlockMover mover;
	private List<Block> initialBlocks;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		 //5x4 board
		 initialBlocks = new ArrayList<Block>();
	}

	@After
	public void tearDown() throws Exception {
		mover = null;
		initialBlocks = null;
	}
	
	@Test
	public void nothingChangesIfAllBlocksAreVisible() {
		for(int i = 0; i < 19; i++) {
			initialBlocks.add(new Block(Colour.ORANGE));
		}
		List<Block> copyOfInitialBlocks = new ArrayList<Block>(initialBlocks);
		mover = new BlockMover(initialBlocks);
		mover.moveBlocksDown();
		assertEquals(copyOfInitialBlocks, initialBlocks);
	}
	
	@Test
	public void oneBlockIsMovedDownToSecondRow() {
		createBoardWithInvisibleBlockInSecondRow();
		mover = new BlockMover(initialBlocks);
		mover.moveBlocksDown();
		assertFalse(initialBlocks.get(3).isVisible());
		for (Block block : initialBlocks) {
			if(!block.equals(initialBlocks.get(3))) {
				assertTrue(block.isVisible());
			}
		}
		assertEquals(Colour.ORANGE, initialBlocks.get(8).getColour());
	}
	
	@Test
	public void twoBlocksAreMovedDownToLastRow() {
		createBoardWithInvisibleBlocksInLastTwoRows();
		mover = new BlockMover(initialBlocks);
		mover.moveBlocksDown();
		assertFalse(initialBlocks.get(0).isVisible());
		assertFalse(initialBlocks.get(5).isVisible());
		for (Block block : initialBlocks) {
			if(!block.equals(initialBlocks.get(0)) && !block.equals(initialBlocks.get(5))) {
				assertTrue(block.isVisible());
			}
		}
		assertEquals(Colour.ORANGE, initialBlocks.get(10).getColour());
		assertEquals(Colour.BLUE, initialBlocks.get(15).getColour());

	}
	
	@Test
	public void condenseBoardOneEmptyColumn(){
		prepareOneEmptyColumn();
		
		mover = new BlockMover(initialBlocks);
		mover.condenseBoard();
		assertFalse(initialBlocks.get(27).isVisible());
		assertTrue(initialBlocks.get(28).isVisible());
		assertEquals(Colour.GREEN, initialBlocks.get(28).getColour());	
		assertTrue(initialBlocks.get(57).isVisible());
		assertEquals(Colour.BLUE, initialBlocks.get(57).getColour());
		assertTrue(initialBlocks.get(88).isVisible());
		assertEquals(Colour.RED, initialBlocks.get(88).getColour());
		assertTrue(initialBlocks.get(87).isVisible());
		assertEquals(Colour.GREEN, initialBlocks.get(87).getColour());
		for (int i = 29; i < initialBlocks.size(); i+=Board.BOARDWIDTH) {
			assertFalse(initialBlocks.get(i).isVisible());
		}
	}
	
	@Test
	public void condenseBoardTwoEmptyColumns(){
		prepareTwoEmptyColumns();
		
		mover = new BlockMover(initialBlocks);
		mover.condenseBoard();
		assertFalse(initialBlocks.get(26).isVisible());
		assertTrue(initialBlocks.get(27).isVisible());
		assertEquals(Colour.GREEN, initialBlocks.get(27).getColour());	
		assertTrue(initialBlocks.get(56).isVisible());
		assertEquals(Colour.BLUE, initialBlocks.get(56).getColour());
		assertTrue(initialBlocks.get(87).isVisible());
		assertEquals(Colour.RED, initialBlocks.get(87).getColour());
		assertTrue(initialBlocks.get(86).isVisible());
		assertEquals(Colour.GREEN, initialBlocks.get(86).getColour());
		for (int i = 29; i < initialBlocks.size(); i+=Board.BOARDWIDTH) {
			assertFalse(initialBlocks.get(i).isVisible());
		}
		for (int i = 28; i < initialBlocks.size(); i+=Board.BOARDWIDTH) {
			assertFalse(initialBlocks.get(i).isVisible());
		}
	}

	
	
	
	// Helper methods
	
	private void createBoardWithInvisibleBlockInSecondRow() {
		for (int i = 0; i < 20; i++) {
			initialBlocks.add(new Block(Colour.GREEN));
		}
		
		Block movable = new Block(Colour.ORANGE);
		initialBlocks.set(3, movable);

		Block invisible = new Block(Colour.RED);
		invisible.setVisible(false);
		initialBlocks.set(8, invisible);
		
		movable.setDownNeighbour(invisible);
	}
	
	private void createBoardWithInvisibleBlocksInLastTwoRows() {
		for (int i = 0; i < 20; i++) {
			initialBlocks.add(new Block(Colour.GREEN));
		}
		
		Block movable1 = new Block(Colour.ORANGE);
		initialBlocks.set(0, movable1);

		Block movable2 = new Block(Colour.BLUE);
		initialBlocks.set(5, movable2);

		Block invisible1 = new Block(Colour.RED);
		invisible1.setVisible(false);
		initialBlocks.set(10, invisible1);
		
		Block invisible2 = new Block(Colour.RED);
		invisible2.setVisible(false);
		initialBlocks.set(15, invisible2);
		
		movable1.setDownNeighbour(movable2);
		movable2.setDownNeighbour(invisible1);
		invisible1.setDownNeighbour(invisible2);
	}
	
	
	private void prepareOneEmptyColumn() {
		for (int i = 0; i < Board.BOARDWIDTH*3; i++) {
			initialBlocks.add(new Block(Colour.GREEN));
		}
		for (Block block : initialBlocks) {
			int index = initialBlocks.indexOf(block);
			if(index%Board.BOARDWIDTH == 27){
				block.setVisible(false);
			}
			else if (index%Board.BOARDWIDTH == 28 || index%Board.BOARDWIDTH == 29) {
				block.setLeftNeighbour(initialBlocks.get(index-1));
			}
		}
		initialBlocks.get(28).setVisible(false);
		initialBlocks.get(58).setColour(Colour.BLUE);
		initialBlocks.get(89).setColour(Colour.RED);
	}
	
	private void prepareTwoEmptyColumns() {
		for (int i = 0; i < Board.BOARDWIDTH*3; i++) {
			initialBlocks.add(new Block(Colour.GREEN));
		}
		for (Block block : initialBlocks) {
			int index = initialBlocks.indexOf(block);
			if(index%Board.BOARDWIDTH == 26 || index%Board.BOARDWIDTH == 27){
				block.setVisible(false);
			}
			if (index%Board.BOARDWIDTH == 27 || index%Board.BOARDWIDTH == 28 || index%Board.BOARDWIDTH == 29) {
				block.setLeftNeighbour(initialBlocks.get(index-1));
			}
		}
		initialBlocks.get(28).setVisible(false);
		initialBlocks.get(58).setColour(Colour.BLUE);
		initialBlocks.get(89).setColour(Colour.RED);
	}

}
