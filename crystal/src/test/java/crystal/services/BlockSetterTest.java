package crystal.services;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import crystal.model.Block;
import crystal.model.Board;
import crystal.model.Colour;

public class BlockSetterTest {
	private BlockSetter setter;
	private List<Block> allBlocks = new ArrayList<Block>();
 
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		while(allBlocks.size() < Board.BOARDLENGTH) {
			allBlocks.add(new Block(Colour.BLUE));
		}
		setter = new BlockSetter(allBlocks);
	}

	@After
	public void tearDown() throws Exception {
		setter = null;
	}

	@Test
	public void testSetNeighboursForUpperLeftCorner() {
		Block upperLeftCorner = allBlocks.get(0);
		setter.setBlockNeighbours(upperLeftCorner);
		
		assertNull(upperLeftCorner.getLeftNeighbour());
		assertNull(upperLeftCorner.getUpNeighbour());
		assertSame(upperLeftCorner.getRightNeighbour(), allBlocks.get(1));
		assertSame(upperLeftCorner.getDownNeighbour(), allBlocks.get(30));
	}
	
	@Test
	public void testSetNeighboursForUpperRightCorner() {
		Block upperRightCorner = allBlocks.get(29);
		setter.setBlockNeighbours(upperRightCorner);
		
		assertSame(upperRightCorner.getLeftNeighbour(),allBlocks.get(28));
		assertNull(upperRightCorner.getUpNeighbour());
		assertNull(upperRightCorner.getRightNeighbour());
		assertSame(upperRightCorner.getDownNeighbour(), allBlocks.get(59));
	}
	
	@Test
	public void testSetNeighboursForLowerLeftCorner() {
		Block lowerLeftCorner = allBlocks.get(570);
		setter.setBlockNeighbours(lowerLeftCorner);
		
		assertNull(lowerLeftCorner.getLeftNeighbour());
		assertSame(lowerLeftCorner.getUpNeighbour(), allBlocks.get(540));
		assertSame(lowerLeftCorner.getRightNeighbour(), allBlocks.get(571));
		assertNull(lowerLeftCorner.getDownNeighbour());
	}
	
	@Test
	public void testSetNeighboursForLowerRightCorner() {
		Block lowerRightCorner = allBlocks.get(599);
		setter.setBlockNeighbours(lowerRightCorner);
		
		assertSame(lowerRightCorner.getLeftNeighbour(),allBlocks.get(598));
		assertSame(lowerRightCorner.getUpNeighbour(), allBlocks.get(569));
		assertNull(lowerRightCorner.getRightNeighbour());
		assertNull(lowerRightCorner.getDownNeighbour());
	}
	
	@Test
	public void testSetNeighboursForMiddleBlock() {
		Block middle = allBlocks.get(123);
		setter.setBlockNeighbours(middle);
		
		assertSame(middle.getLeftNeighbour(),allBlocks.get(122));
		assertSame(middle.getUpNeighbour(), allBlocks.get(93));
		assertSame(middle.getRightNeighbour(), allBlocks.get(124));
		assertSame(middle.getDownNeighbour(), allBlocks.get(153));
	}
}
