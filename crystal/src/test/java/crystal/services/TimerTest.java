package crystal.services;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import crystal.model.CurrentGame;

public class TimerTest {
	Timer timer;
	CurrentGame game;

	@Before
	public void setUp() throws Exception {
		game = new CurrentGame();
		timer = new Timer();
		game.setCurrentTime(timer);
	}

	@After
	public void tearDown() throws Exception {
		timer = null;
		game = null;
	}

	@Test
	public void testMinutesLess10() {
		timer.setSeconds(130);
		assertEquals("02:10", timer.timeToMinutes());
	}
	
	@Test
	public void testSecondsLess10() {
		timer.setSeconds(129);
		assertEquals("02:09", timer.timeToMinutes());
	}
	
	@Test
	public void testNothingLess10() {
		timer.setSeconds(610);
		assertEquals("10:10", timer.timeToMinutes());
	}

	@Test
	public void testIncrementTime() {
		timer.setSeconds(129);
		timer.incrementTime();
		assertEquals("02:10", timer.timeToMinutes());
	}

}
