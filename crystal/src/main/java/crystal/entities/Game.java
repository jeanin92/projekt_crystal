package crystal.entities;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Game {
	
	@Id
	private int id;
	protected int rank;
	protected String user;
	protected int totalPoints;
	protected int level;
	protected Date date;
	
	public Game() {
	}
	
	public Game(String user, int points, int level, Date date) {
		this.user = user;
		this.totalPoints = points;
		this.level = level;
		this.date = date;
	}	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public int getTotalPoints() {
		return totalPoints;
	}
	public void setTotalPoints(int points) {
		this.totalPoints = points;
	}
	public int getLevel() {
		return level;
	}
	public void setLevel(int level) {
		this.level = level;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}
	
	
	
}
