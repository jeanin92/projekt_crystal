package crystal.controller;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.context.RequestContext;

import crystal.model.Block;
import crystal.model.CurrentGame;
import crystal.model.Level;
import crystal.services.BlockMover;
import crystal.services.BlockRemover;
import crystal.services.BoardSetter;
import crystal.services.GameStatusUpdater;

@ManagedBean
@SessionScoped
public class GameController implements Serializable {
	private static final long serialVersionUID = -8948714689297825121L;
	private static final String LEVELNAME = "Level ";
	public static final String FORCEPAGEREDIRECT = "?faces-redirect=true";
	
	private CurrentGame game;
	private BoardSetter boardSetter;
	private BlockRemover remover;
	private BlockMover mover;
	private GameStatusUpdater statusUpdater;
	private boolean levelGoalPopupVisible;
	
	
	@PostConstruct
	public void init() {
		createNewGame();	
	}
	
	private void createNewGame(){
		game = new CurrentGame();
		this.boardSetter = new BoardSetter();
		game.setBoard(boardSetter.initBoard(game.getCurrentLevel()));
		this.statusUpdater = new GameStatusUpdater(game);
		statusUpdater.setStatusOfLevel();
		this.levelGoalPopupVisible = true;
	}
	
	public String goToGamePage() {
		createNewGame();
		return "game"+FORCEPAGEREDIRECT;
	}
	
	
	public String cancelGame(){
		if(game.getCurrentLevel() != Level.LEVEL1) {
			int previousLevelNumber = game.getCurrentLevel().getLevelNumber()-1;
			game.setCurrentLevel(Level.valueOf("LEVEL" + previousLevelNumber));
			return "finalResult"+FORCEPAGEREDIRECT;
		}
		return "noFinishedLevel" + FORCEPAGEREDIRECT;
	}
	
	public String goToIndex() {
		return "index"+FORCEPAGEREDIRECT;
	}
	
	public String startNewLevel(){
		statusUpdater.setStatusOfLevel();
		statusUpdater.incrementLevel();
		this.game.setBoard(boardSetter.initBoard(this.game.getCurrentLevel()));
		this.levelGoalPopupVisible = true;
		return "game"+FORCEPAGEREDIRECT;
	}
	
	public void onClick(Block block){
		if(block.isClickable()){
			remover = new BlockRemover();
			int removedBlocks = remover.removeSameColouredNeighboursOf(block);
			mover = new BlockMover(game.getBoard().getGameBoard());
			mover.moveBlocksDown();
			mover.condenseBoard();
			statusUpdater.updatePoints(removedBlocks);
			statusUpdater.updateBlockCount(removedBlocks);
			if(!checkMovesPossible()){
				statusUpdater.finishLevel();
				RequestContext.getCurrentInstance().execute("gameFinished.show()");
			}
		}		
	}
	
	private boolean checkMovesPossible(){
		for (Block block : this.game.getBoard().getGameBoard()) {
			if(block.isClickable()){
				return true;
			}
		}
		return false;
	}	

	public String hideLevelGoalPopup() {
		this.levelGoalPopupVisible = false;
		return "game"+FORCEPAGEREDIRECT;
	}
	
	public String goToResult() {
		if(!this.game.wins() || this.game.getCurrentLevel().isLastLevel()){
			return "finalResult" +FORCEPAGEREDIRECT;
		}
		return "levelResult" +FORCEPAGEREDIRECT;
	}

	public String returnPreviousLevelName(){
		int levelNumber = this.game.getCurrentLevel().getLevelNumber();
		return LEVELNAME + (levelNumber-1);
	}	
	
	public String returnNextLevelName() {
		int levelNumber = this.game.getCurrentLevel().getLevelNumber();
		return LEVELNAME + (levelNumber+1);
	}
	
	
	// Getter and Setter
	
	public boolean getLevelGoalPopupVisible() {
		return levelGoalPopupVisible;
	}

	public CurrentGame getGame() {
		return game;
	}
}