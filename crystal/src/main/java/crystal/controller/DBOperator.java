package crystal.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import crystal.entities.Game;
import crystal.model.CurrentGame;

@ManagedBean
@SessionScoped
public class DBOperator implements Serializable{
	private static final long serialVersionUID = 5964792832882792637L;
	private Session session;
	private SessionFactory factory;
	private Game game;

	public DBOperator() {
	}
	
	public DBOperator(String config) {
		factory = new Configuration()
		.configure(config).buildSessionFactory();
		session = factory.openSession();
		game = new Game();
	}
	
	@PostConstruct
	public void init(){
		factory = new Configuration()
		.configure("hibernate.cfg.xml").buildSessionFactory();
		session = factory.openSession();
		game = new Game();
	}
	
	@SuppressWarnings("unchecked")
	public List<Game> getRanking(){
		session.beginTransaction();
		String sqlQuery = "from Game order by points desc";
		Query query = session.createQuery(sqlQuery);
		List<Game> games = query.list();
		session.getTransaction().commit();		
		return addRank(games);
	}

	private List<Game> addRank(List<Game> games) {
			int rank = 0;
			int points = 0;			
			for (Game game : games) {
				if(points != game.getTotalPoints()) {
					rank++;
				}
				game.setRank(rank);
				points = game.getTotalPoints();
			}
			return games;
	}
	
	public String saveGame(CurrentGame currentGame){
			java.sql.Date currentDate = new java.sql.Date(new Date().getTime());
			this.game.setDate(currentDate);
			this.game.setLevel(currentGame.getCurrentLevel().getLevelNumber());
			this.game.setTotalPoints(currentGame.getTotalPoints());
			session.beginTransaction();
			session.save(game);
			session.getTransaction().commit();
			game = new Game();
			return "ranking"+GameController.FORCEPAGEREDIRECT;
	}

	public Session getSession() {
		return session;
	}

	public SessionFactory getFactory() {
		return factory;
	}

	public Game getGame() {
		return game;
	}
}
