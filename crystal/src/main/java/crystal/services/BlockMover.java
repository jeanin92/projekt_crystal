package crystal.services;

import java.util.List;

import crystal.model.Block;
import crystal.model.Board;

public class BlockMover {

	private List<Block> gameBoardBlocks;
	
	public BlockMover(List<Block> gameBoardBlocks) {
		this.gameBoardBlocks = gameBoardBlocks;
	}
	
	public void moveBlocksDown() {
		boolean verified = true;
		while(verified) {
			verified = false;
			for (Block block : gameBoardBlocks) {
				if(block.isVisible() && block.getDownNeighbour() != null 
						&& !block.getDownNeighbour().isVisible()) {
					verified = true;
					block.getDownNeighbour().setColour(block.getColour());
					block.getDownNeighbour().setVisible(true);
					block.setVisible(false);
				}
			}
		}
	}
	
	public void condenseBoard() {
		int boardLength = gameBoardBlocks.size();
		for(int i = boardLength-1; i >= boardLength-Board.BOARDWIDTH; i--) {
			if(!gameBoardBlocks.get(i).isVisible()) {
				moveAllColumnsFromRight(i);
			}
		}
	}

	private void moveAllColumnsFromRight(int i) {
		int columnIndex = i % Board.BOARDWIDTH;
		for (Block block : gameBoardBlocks) {
			if(gameBoardBlocks.indexOf(block) % Board.BOARDWIDTH > columnIndex) {
				block.getLeftNeighbour().setColour(block.getColour());
				block.getLeftNeighbour().setVisible(block.isVisible());
				block.setVisible(false);
			}
		}
	}
}
