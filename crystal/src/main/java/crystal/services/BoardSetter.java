package crystal.services;

import java.util.ArrayList;
import java.util.List;
import crystal.model.Block;
import crystal.model.Board;
import crystal.model.Level;

public class BoardSetter {

	private List<Block> gameBoardBlocks;
	private ColourRandomizer colRandomizer;

	public BoardSetter() {
		colRandomizer = new ColourRandomizer();
	}
	
	public Board initBoard(Level level){
		colRandomizer.randomizePoolOfColours(level.getColourCount());
		gameBoardBlocks = generateBlockArray();
		setAllNeighboursForBlocks();
		return new Board(gameBoardBlocks);
	}


	private List<Block> generateBlockArray(){
		gameBoardBlocks = new ArrayList<Block>();
		while(gameBoardBlocks.size() < Board.BOARDLENGTH) {
			gameBoardBlocks.add(new Block(colRandomizer.randomizeColour()));
		}
		return gameBoardBlocks;
	}

	private void setAllNeighboursForBlocks() {
		BlockSetter blockSetter = new BlockSetter(gameBoardBlocks);
		for (Block block : gameBoardBlocks) {
			blockSetter.setBlockNeighbours(block);
		}
	}

}
