package crystal.services;

import crystal.model.Block;
import crystal.model.Colour;

public class BlockRemover {
	
	public int removeSameColouredNeighboursOf(Block block){
		int removedBlocks = 1;
		Colour colour = block.getColour();
		block.setVisible(false);
		removedBlocks += checkForSameColouredNeighbours(block.getUpNeighbour(), colour);
		removedBlocks += checkForSameColouredNeighbours(block.getRightNeighbour(), colour);
		removedBlocks += checkForSameColouredNeighbours(block.getDownNeighbour(), colour);
		removedBlocks += checkForSameColouredNeighbours(block.getLeftNeighbour(), colour);
		return removedBlocks;
	}
	
	private int checkForSameColouredNeighbours(Block block, Colour colour){
		if(block != null && 
				block.isVisible() && 
				block.getColour() == colour){
			return removeSameColouredNeighboursOf(block);
		}
		return 0;
	}
}