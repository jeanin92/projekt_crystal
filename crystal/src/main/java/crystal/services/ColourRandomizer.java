package crystal.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import crystal.model.Colour;

public class ColourRandomizer {

	private static final int FIRSTCOLOURINPOOL = 0;
	
	private List<Colour> poolOfColours;
	
	public List<Colour> randomizePoolOfColours(int colourCount) {
		this.poolOfColours = new ArrayList<Colour>();
		Colour randomColour;
		while(poolOfColours.size() < colourCount) {
			randomColour = Colour.getRandomColour();
			if(!poolOfColours.contains(randomColour)){
				poolOfColours.add(randomColour);
			}			
		}
		return this.poolOfColours;
	}
	
	public Colour randomizeColour() {
		Collections.shuffle(poolOfColours);
		return poolOfColours.get(FIRSTCOLOURINPOOL);
	}
}
