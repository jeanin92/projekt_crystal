package crystal.services;

import crystal.model.Board;
import crystal.model.CurrentGame;
import crystal.model.Level;

public class GameStatusUpdater {
	private static final double EXPONENT = 1.55;
	private static final int BONUS = 200;
	
	CurrentGame game;
	
	public GameStatusUpdater(CurrentGame game) {
		this.game = game;
 	}
	
	public void updatePoints(int removedBlocks){
		int points = (int) Math.floor(Math.pow(removedBlocks, EXPONENT));
		this.game.setCurrentPoints(this.game.getCurrentPoints()+points);
	}
	
	public void updateBlockCount(int removedBlocks){
		this.game.setCurrentNumberOfBlocks(this.game.getCurrentNumberOfBlocks()-removedBlocks);
	}
	
	public void finishLevel(){
		this.game.setFinished(true);
		addBonusPoints();
		if(this.game.wins()){
			this.game.setTotalPoints(this.game.getTotalPoints()+this.game.getCurrentPoints());
		}
	}
	
	private void addBonusPoints(){
		if(this.game.getCurrentNumberOfBlocks()==0){
			this.game.setCurrentPoints(this.game.getCurrentPoints()+BONUS);
		}
	}
	
	public void setStatusOfLevel() {
		this.game.setCurrentNumberOfBlocks(Board.BOARDLENGTH);
		this.game.setCurrentPoints(0);
		this.game.setFinished(false);
		this.game.getCurrentTime().resetTimer();
	}
	
	public void incrementLevel(){
		if(this.game.getCurrentLevel() != Level.LEVEL5) {
			int newLevel = this.game.getCurrentLevel().getLevelNumber()+1;		
			this.game.setCurrentLevel(Level.valueOf("LEVEL"+newLevel));	
		}
	}

}
