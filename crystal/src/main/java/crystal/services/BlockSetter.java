package crystal.services;

import java.util.List;

import crystal.model.Block;
import crystal.model.Board;

public class BlockSetter {
	
	private List<Block> gameBoardBlocks;
	private int currentBlockIndex;
	private Block block;
	
	public BlockSetter(List<Block> gameBoardBlocks) {
		this.gameBoardBlocks = gameBoardBlocks;
	}
	
	public void setBlockNeighbours(Block block) {
		this.block = block;
		currentBlockIndex = gameBoardBlocks.indexOf(block);
		setNeighbourAbove();
		setNeighbourOnTheRight();
		setNeighbourBelow();
		setNeighbourOnTheLeft();
	}

	private void setNeighbourAbove() {
		if(currentBlockIndex >= Board.BOARDWIDTH) {
			block.setUpNeighbour(gameBoardBlocks.get(currentBlockIndex - Board.BOARDWIDTH));
		}
	}
	
	private void setNeighbourOnTheRight() {
		if((currentBlockIndex % Board.BOARDWIDTH) != (Board.BOARDWIDTH - 1)) {
			block.setRightNeighbour(gameBoardBlocks.get(currentBlockIndex + 1));
		}
	}
	
	private void setNeighbourBelow() {
		if(currentBlockIndex < (Board.BOARDLENGTH - Board.BOARDWIDTH)) {
			block.setDownNeighbour(gameBoardBlocks.get(currentBlockIndex + Board.BOARDWIDTH));
		}
	}
	
	private void setNeighbourOnTheLeft() {
		if(currentBlockIndex % Board.BOARDWIDTH != 0) {
			block.setLeftNeighbour(gameBoardBlocks.get(currentBlockIndex - 1));
		}
	}
}
