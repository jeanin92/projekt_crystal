package crystal.services;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;


public class Timer {
	private static final int TIMEOUT = 1800;

	private int timeInSeconds;
	
	public Timer() {
		timeInSeconds = 0;
	}
	
	public String timeToMinutes() {
		String minutes = String.valueOf((int)Math.floor(this.timeInSeconds/60));
		String seconds = String.valueOf(this.timeInSeconds % 60);
		if(minutes.length() < 2) {
			minutes = "0" + minutes;
		}
		if(seconds.length() < 2) {
			seconds = "0" + seconds;
		}
		return minutes + ":" + seconds;
	}

	public void incrementTime() {
		timeInSeconds++;
		checkTimeout();
	}
	
	private void checkTimeout(){
		if(timeInSeconds>=TIMEOUT){
			RequestContext.getCurrentInstance().execute("sessionTimeout.show();");
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			ec.invalidateSession();
		}
	}

	public void resetTimer() {
		timeInSeconds = 0;
	}
	
	public void setSeconds(int timeInSeconds) {
		this.timeInSeconds = timeInSeconds;
	}
}
