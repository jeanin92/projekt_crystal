package crystal.model;

public enum Level {
//	LEVEL0(2, 1000),
	LEVEL1(3, 1500),
	LEVEL2(3, 2000),
	LEVEL3(4, 1200),
	LEVEL4(4, 1500),
	LEVEL5(5, 1000);
	
	private int colourCount;
	private int goal;
	
	private Level(int colourCount, int goal){
		this.colourCount = colourCount;
		this.goal = goal;
	}
	
	public String toString() {
		return "Level "+ getLevelNumber();
	}
	
	public int getLevelNumber() {
		return Integer.parseInt(this.name().substring(5));	
	}
	
	public boolean isLastLevel(){
		if(this.getLevelNumber() == Level.values().length){
			return true;
		}
		return false;		
	}
	
	public int getGoal() {
		return this.goal;
	}

	public int getColourCount() {
		return colourCount;
	}
}
