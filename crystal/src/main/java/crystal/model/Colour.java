package crystal.model;

import java.util.Random;

public enum Colour {
	RED(),
	BLUE(),
	YELLOW(),
	GREEN(),
	ORANGE();
	
	private static final Colour[] VALUES = values();
	private static final int LENGTH = VALUES.length;
	private static Random randomizer = new Random();

	public static Colour getRandomColour() {
		return VALUES[randomizer.nextInt(LENGTH)];
	}
	
	public String getStyle(){
		return this.name();
	}
}
