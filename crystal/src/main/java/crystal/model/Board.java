package crystal.model;

import java.util.List;

public class Board {
	public static final int BOARDWIDTH = 30;
	public static final int BOARDHEIGHT = 20;
	public static final int BOARDLENGTH = BOARDWIDTH*BOARDHEIGHT;

	private List<Block> gameBoardBlocks;
	
	public Board(List<Block> gameBoardBlocks) {
		this.gameBoardBlocks = gameBoardBlocks;
	}

	public List<Block> getGameBoard() {
		return gameBoardBlocks;
	}
	
	public void setGameBoard(List<Block> gameBoardBlocks) {
		this.gameBoardBlocks = gameBoardBlocks;
	}
	
	public int getBoardWidth() {
		return BOARDWIDTH;
	}
	
}
