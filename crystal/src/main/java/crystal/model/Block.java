package crystal.model;

public class Block {
	private Colour colour;
	private Block leftNeighbour = null;
	private Block rightNeighbour = null;
	private Block upNeighbour = null;
	private Block downNeighbour = null;
	private boolean visible = true;
	
	public Block(Colour colour) {
		this.colour = colour;
	}
	
	public boolean isClickable(){
		if(!this.visible){
			return false;
		}
		if(leftNeighbourIsSame() || rightNeighbourIsSame()
				|| upNeighbourIsSame() || downNeighbourIsSame()) {
			return true;
		}
		return false;
	}
				
	private boolean leftNeighbourIsSame() {
		if(leftNeighbour != null && leftNeighbour.visible &&
				leftNeighbour.colour == this.colour) {
			return true;
		}
		return false;
	}
	
	private boolean rightNeighbourIsSame() {
		if(rightNeighbour != null && rightNeighbour.visible &&
				rightNeighbour.colour == this.colour) {
			return true;
		}
		return false;
	}
	
	private boolean upNeighbourIsSame() {
		if(upNeighbour != null && upNeighbour.visible &&
				upNeighbour.colour == this.colour) {
			return true;
		}
		return false;
	}
	
	private boolean downNeighbourIsSame() {
		if(downNeighbour != null && downNeighbour.visible &&
				downNeighbour.colour == this.colour) {
			return true;
		}
		return false;
	}
	

	// Getter and setter	
	public Colour getColour() {
		return colour;
	}
	
	public void setColour(Colour colour) {
		this.colour = colour;
	}
	
	public Block getLeftNeighbour() {
		return leftNeighbour;
	}

	public void setLeftNeighbour(Block leftNeighbour) {
		this.leftNeighbour = leftNeighbour;
	}

	public Block getRightNeighbour() {
		return rightNeighbour;
	}

	public void setRightNeighbour(Block rightNeighbour) {
		this.rightNeighbour = rightNeighbour;
	}

	public Block getUpNeighbour() {
		return upNeighbour;
	}

	public void setUpNeighbour(Block upNeighbour) {
		this.upNeighbour = upNeighbour;
	}

	public Block getDownNeighbour() {
		return downNeighbour;
	}

	public void setDownNeighbour(Block downNeighbour) {
		this.downNeighbour = downNeighbour;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean removed) {
		this.visible = removed;
	}
}
