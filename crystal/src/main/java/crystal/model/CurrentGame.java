package crystal.model;

import crystal.services.Timer;


public class CurrentGame{
	
	private int currentPoints;
	private Level currentLevel;
	private Timer currentTime;
	private int totalPoints;
	private int currentNumberOfBlocks;
	private Board board;
	private boolean finished;
	
	
	public CurrentGame() {
		this.totalPoints = 0;
		this.currentLevel = Level.LEVEL1;
		this.currentTime = new Timer();
	}	

	public boolean wins(){
		if(this.currentPoints>=this.currentLevel.getGoal()){
			return true;
		}
		return false;
	}	
	
	//Getter and setter
	
	public Timer getCurrentTime() {
		return currentTime;
	}
	
	public void setCurrentTime(Timer currentTime) {
		this.currentTime = currentTime;
	}
	
	public int getCurrentPoints() {
		return this.currentPoints;
	}
	
	public Level getCurrentLevel() {
		return this.currentLevel;
	}

	public int getCurrentNumberOfBlocks() {
		return currentNumberOfBlocks;
	}

	public Board getBoard() {
		return board;
	}
	
	public void setBoard(Board board) {
		this.board = board;
	}

	public void setCurrentPoints(int currentPoints) {
		this.currentPoints = currentPoints;
	}

	public void setCurrentLevel(Level currentLevel) {
		this.currentLevel = currentLevel;
	}

	public void setCurrentNumberOfBlocks(int currentNumberOfBlocks) {
		this.currentNumberOfBlocks = currentNumberOfBlocks;
	}

	public boolean isFinished() {
		return finished;
	}


	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	public int getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(int totalPoints) {
		this.totalPoints = totalPoints;
	}
}
